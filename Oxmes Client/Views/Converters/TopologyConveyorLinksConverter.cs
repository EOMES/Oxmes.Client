﻿using Oxmes.Client.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Oxmes.Client.Views.Converters
{
    public class TopologyConveyorLinksConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IDictionary<int, IDictionary<int, int>> dictType)
            {
                var labelList = new List<ConveyorLink>();
                foreach (var top in dictType)
                {
                    foreach (var bottom in top.Value)
                    {
                        var source = top.Key;
                        var port = bottom.Key;
                        var destination = bottom.Value;
                        labelList.Add(new ConveyorLink(source, port, destination));
                    }
                }
                return labelList;
            }
            else
            {
                throw new NotImplementedException($"Unknown conveyor link type {value.GetType()}");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
