﻿using Oxmes.Client.Services;
using Oxmes.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oxmes.Client.Views
{
    /// <summary>
    /// Interaction logic for TopologyPropertiesPage.xaml
    /// </summary>
    public partial class TopologyPropertiesPage : Page
    {
        private TopologyViewModel TopologyViewModel => DataContext as TopologyViewModel;

        public TopologyPropertiesPage()
        {
            InitializeComponent();
        }

        private void ConveyorLinksList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1 && e.AddedItems[0] is ConveyorLink cl)
            {
                TopologyViewModel.SelectConveyorLink(cl);
            }
        }
    }
}
