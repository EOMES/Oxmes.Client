﻿using Newtonsoft.Json;
using Oxmes.Topology.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Models
{
    public class ModuleConveyorEx : ModuleConveyor
    {
        [JsonIgnore]
        public bool IsDirty { get; set; } = false;

        [JsonIgnore]
        public bool IsNew { get; set; } = false;

        [JsonIgnore]
        public bool IsLoaded { get; set; } = false;

        public ModuleConveyorEx()
        {
        }

        public ModuleConveyorEx(ModuleConveyorEx moduleConveyor)
        {
            ModuleConveyorId = moduleConveyor.ModuleConveyorId;
            Resource = moduleConveyor.Resource;
            ResourceId = moduleConveyor.ResourceId;
            IsDirty = moduleConveyor.IsDirty;
            IsLoaded = moduleConveyor.IsLoaded;
            IsNew = moduleConveyor.IsNew;
        }
    }
}