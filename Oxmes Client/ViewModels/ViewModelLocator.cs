/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Oxmes_Client"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Oxmes.Client.Services;
using System.IO;
//using Microsoft.Practices.ServiceLocation;

namespace Oxmes.Client.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<IConfiguration>(() =>
            {
                return new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
            });
            SimpleIoc.Default.Register<ILoggerFactory>(() =>
            {
                var config = SimpleIoc.Default.GetInstance<IConfiguration>();
                return new LoggerFactory()
                    .AddConsole(config)
                    .AddDebug(LogLevel.Debug);
            });
            SimpleIoc.Default.Register<GatewayService>();
            SimpleIoc.Default.Register<TopologyDataService>();
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<TopologyViewModel>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public TopologyViewModel TopologyViewModel => ServiceLocator.Current.GetInstance<TopologyViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}