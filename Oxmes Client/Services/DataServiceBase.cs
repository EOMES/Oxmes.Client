﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Services
{
    public class DataServiceBase
    {
        protected readonly GatewayService GatewayService;
        private readonly ILogger Logger;

        public DataServiceBase(GatewayService gatewayService, ILoggerFactory loggerFactory)
        {
            GatewayService = gatewayService;
            Logger = loggerFactory.CreateLogger<DataServiceBase>();
        }

        protected async Task GetRequestAsync<T>(string location, Func<HttpResponseMessage, T> requestFunc)
        {
            try
            {
                using (var response = await GatewayService.GatewayHttpClient.GetAsync(location))
                {
                    requestFunc.Invoke(response);
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.LogWarning($"Unable to contact gateway. Reason: {ex.Message}");
            }
        }

        protected async Task PutRequestAsync<T>(string location, T tObject, Action<HttpResponseMessage> requestFunc)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(tObject), Encoding.UTF8, "application/json");
                using (var response = await GatewayService.GatewayHttpClient.PutAsync(location, content))
                {
                    requestFunc.Invoke(response);
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.LogWarning($"Unable to contact gateway. Reason: {ex.Message}");
            }
        }

        protected async Task PostRequestAsync<T>(string location, T tObject, Action<HttpResponseMessage> requestFunc)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(tObject), Encoding.UTF8, "application/json");
                using (var response = await GatewayService.GatewayHttpClient.PostAsync(location, content))
                {
                    requestFunc.Invoke(response);
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.LogWarning($"Unable to contact gateway. Reason: {ex.Message}");
            }
        }

        protected async Task DeleteRequestAsync(string location, Action<HttpResponseMessage> requestFunc)
        {
            try
            {
                using (var response = await GatewayService.GatewayHttpClient.DeleteAsync(location))
                {
                    requestFunc.Invoke(response);
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.LogWarning($"Unable to contact gateway. Reason: {ex.Message}");
            }
        }
    }
}