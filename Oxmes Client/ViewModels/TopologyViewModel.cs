﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Oxmes.Client.Models;
using Oxmes.Client.Services;
using Oxmes.Topology.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Oxmes.Client.ViewModels
{
    public class TopologyViewModel : ViewModelBase
    {
        #region Properties

        private TopologyEx _topology;
        private TopologyEx _WorkingCopyTopology;
        private ModuleConveyorEx _selectedTopologyModuleConveyor;
        private ModuleConveyorEx _workingCopyTopologyModuleConveyor;
        private ModuleConveyorEx _selectedModuleConveyor;
        private ModuleConveyorEx _workingCopyModuleConveyor;
        private Services.ConveyorLink _selectedConveyorLink;
        private Services.ConveyorLink _WorkingCopyConveyorLink;
        private ResourceEx _selectedResource;
        private ResourceEx _workingCopyResource;
        private ApplicationEx _selectedApplication;
        private ApplicationEx _workingCopyApplication;
        private ObservableCollection<Services.ConveyorLink> _conveyorLinks;
        private ObservableCollection<ModuleConveyor> _ModuleConveyorsInTopology;
        private ObservableCollection<ModuleConveyor> _ModuleConveyorsNotInTopology;
        private ApplicationOperation _selectedApplicationOperation;
        private Range _selectedOperationRange;
        private RangeParameter _selectedRangeParameter;
        private ObservableCollection<ApplicationOperation> _applicationOperations;
        private ObservableCollection<Range> _operationRanges;
        private ObservableCollection<RangeParameter> _rangeParameters;
        private readonly GatewayService gatewayService;
        private readonly TopologyDataService topologyDataService;

        public ObservableCollection<TopologyEx> Topologies => topologyDataService.Topologies;
        public ObservableCollection<ModuleConveyorEx> ModuleConveyors => topologyDataService.ModuleConveyors;
        public ObservableCollection<ResourceEx> Resources => topologyDataService.Resources;
        public ObservableCollection<ApplicationEx> Applications => topologyDataService.Applications;

        public ObservableCollection<Services.ConveyorLink> ConveyorLinks { get => _conveyorLinks; set => Set(nameof(ConveyorLinks), ref _conveyorLinks, value); }

        public ObservableCollection<ApplicationOperation> ApplicationOperations { get => _applicationOperations; set => Set(nameof(ApplicationOperations), ref _applicationOperations, value); }
        public ObservableCollection<Range> OperationRanges { get => _operationRanges; set => Set(nameof(OperationRanges), ref _operationRanges, value); }
        public ObservableCollection<RangeParameter> RangeParameters { get => _rangeParameters; set => Set(nameof(RangeParameters), ref _rangeParameters, value); }

        public TopologyEx SelectedTopology { get => _topology; set => Set(nameof(SelectedTopology), ref _topology, value); }
        public TopologyEx WorkingCopyTopology { get => _WorkingCopyTopology; set => Set(nameof(WorkingCopyTopology), ref _WorkingCopyTopology, value); }
        public Services.ConveyorLink SelectedConveyorLink { get => _selectedConveyorLink; set => Set(nameof(SelectedConveyorLink), ref _selectedConveyorLink, value); }
        public Services.ConveyorLink WorkingCopyConveyorLink { get => _WorkingCopyConveyorLink; set => Set(nameof(WorkingCopyConveyorLink), ref _WorkingCopyConveyorLink, value); }
        public ModuleConveyorEx SelectedTopologyModuleConveyor { get => _selectedTopologyModuleConveyor; set => Set(nameof(SelectedTopologyModuleConveyor), ref _selectedTopologyModuleConveyor, value); }
        public ModuleConveyorEx WorkingCopyTopologyModuleConveyor { get => _workingCopyTopologyModuleConveyor; set => Set(nameof(WorkingCopyTopologyModuleConveyor), ref _workingCopyTopologyModuleConveyor, value); }
        public ModuleConveyorEx SelectedModuleConveyor { get => _selectedModuleConveyor; set => Set(nameof(SelectedModuleConveyor), ref _selectedModuleConveyor, value); }
        public ModuleConveyorEx WorkingCopyModuleConveyor { get => _workingCopyModuleConveyor; set => Set(nameof(WorkingCopyModuleConveyor), ref _workingCopyModuleConveyor, value); }
        public ResourceEx SelectedResource { get => _selectedResource; private set => Set(nameof(SelectedResource), ref _selectedResource, value); }
        public ResourceEx WorkingCopyResource { get => _workingCopyResource; private set => Set(nameof(WorkingCopyResource), ref _workingCopyResource, value); }
        public ApplicationEx SelectedApplication { get => _selectedApplication; private set => Set(nameof(SelectedApplication), ref _selectedApplication, value); }
        public ApplicationEx WorkingCopyApplication { get => _workingCopyApplication; private set => Set(nameof(WorkingCopyApplication), ref _workingCopyApplication, value); }
        public ApplicationOperation SelectedApplicationOperation { get => _selectedApplicationOperation; set => Set(nameof(SelectedApplicationOperation), ref _selectedApplicationOperation, value); }
        public Range SelectedOperationRange { get => _selectedOperationRange; set => Set(nameof(SelectedOperationRange), ref _selectedOperationRange, value); }
        public RangeParameter SelectedRangeParameter { get => _selectedRangeParameter; set => Set(nameof(SelectedRangeParameter), ref _selectedRangeParameter, value); }

        public int? ModuleConveyorResourceIndex => Resources?.IndexOf(Resources?.FirstOrDefault(r => r.ResourceId == WorkingCopyModuleConveyor?.ResourceId)) + 1;
        public int? ResourceApplicationIndex => Applications?.IndexOf(Applications?.FirstOrDefault(a => a.ApplicationId == WorkingCopyResource?.ApplicationId)) + 1;
        public ObservableCollection<ModuleConveyor> ModuleConveyorsInTopology { get => _ModuleConveyorsInTopology; set => Set(nameof(ModuleConveyorsInTopology), ref _ModuleConveyorsInTopology, value); }
        public ObservableCollection<ModuleConveyor> ModuleConveyorsNotInTopology { get => _ModuleConveyorsNotInTopology; set => Set(nameof(ModuleConveyorsNotInTopology), ref _ModuleConveyorsNotInTopology, value); }

        #endregion Properties

        #region Commands

        public RelayCommand AddModuleConveyorInTopologyCommand { get; }
        public RelayCommand ApplyConveyorLinkChangeCommand { get; }
        public RelayCommand CreateConveyorLinkCommand { get; }
        public RelayCommand CreateTopologyCommand { get; }
        public RelayCommand CreateModuleConveyorCommand { get; }
        public RelayCommand CreateResourceCommand { get; }
        public RelayCommand CreateApplicationCommand { get; }
        public RelayCommand CreateApplicationOperationCommand { get; }
        public RelayCommand CreateOperationRangeCommand { get; }
        public RelayCommand CreateRangeParameterCommand { get; }
        public RelayCommand DeleteConveyorLinkCommand { get; }
        public RelayCommand DeleteTopologyCommand { get; }
        public RelayCommand DeleteModuleConveyorCommand { get; }
        public RelayCommand DeleteResourceCommand { get; }
        public RelayCommand DeleteApplicationCommand { get; }
        public RelayCommand DeleteApplicationOperationCommand { get; }
        public RelayCommand DeleteOperationRangeCommand { get; }
        public RelayCommand DeleteRangeParameterCommand { get; }
        public RelayCommand SaveTopologyCommand { get; }
        public RelayCommand SaveModuleConveyorCommand { get; }
        public RelayCommand SaveResourceCommand { get; }
        public RelayCommand SaveApplicationCommand { get; }
        public RelayCommand RemoveModuleConveyorInTopologyCommand { get; }
        public RelayCommand RefreshCommand { get; }
        public RelayCommand RevertConveyorLinkChangeCommand { get; }
        public RelayCommand RevertTopologyChangesCommand { get; }
        public RelayCommand RevertModuleConveyorChangesCommand { get; }
        public RelayCommand RevertResourcesChangesCommand { get; }
        public RelayCommand RevertApplicationChangesCommand { get; }
        public RelayCommand<object> WorkingCopyConveyorResourceChangedCommand { get; }
        public RelayCommand<object> WorkingCopyResourceApplicationChangedCommand { get; }
        public RelayCommand<object> SelectedApplicationOperationChangedCommand { get; }
        public RelayCommand<object> SelectedOperationRangeChangedCommand { get; }
        public RelayCommand<object> SelectedRangeParameterChangedCommand { get; }
        public ICommand TopologyNameChangedCommand { get; }
        public ICommand ResourceDetailsChangedCommand { get; }
        public ICommand ApplicationDetailsChangedCommand { get; }

        #endregion Commands

        public TopologyViewModel(GatewayService gatewayService, TopologyDataService topologyDataService)
        {
            this.gatewayService = gatewayService;
            this.topologyDataService = topologyDataService;

            AddModuleConveyorInTopologyCommand = new RelayCommand(AddModuleConveyorInTopology, CanAddModuleConveyorInTopology);
            ApplyConveyorLinkChangeCommand = new RelayCommand(UpdateConveyorLink, ConveyorLinkChangeCanExecute);
            CreateConveyorLinkCommand = new RelayCommand(CreateConveyorLink);
            CreateTopologyCommand = new RelayCommand(async () => await CreateTopologyAsync());
            CreateModuleConveyorCommand = new RelayCommand(CreateModuleConveyor);
            CreateResourceCommand = new RelayCommand(CreateResource);
            CreateApplicationCommand = new RelayCommand(CreateApplication);
            CreateApplicationOperationCommand = new RelayCommand(CreateApplicationOperation);
            CreateOperationRangeCommand = new RelayCommand(CreateOperationRange, () => SelectedApplicationOperation != null);
            CreateRangeParameterCommand = new RelayCommand(CreateRangeParameter, () => SelectedOperationRange != null);
            DeleteTopologyCommand = new RelayCommand(async () => await DeleteTopologyAsync(), () => SelectedTopology != null);
            DeleteModuleConveyorCommand = new RelayCommand(DeleteModuleConveyorAsync, () => SelectedModuleConveyor != null);
            DeleteResourceCommand = new RelayCommand(DeleteResourceAsync, () => SelectedResource != null);
            DeleteApplicationCommand = new RelayCommand(DeleteApplicationAsync, () => SelectedApplication != null);
            DeleteApplicationOperationCommand = new RelayCommand(DeleteApplicationOperationAsync, () => SelectedApplicationOperation != null);
            DeleteOperationRangeCommand = new RelayCommand(DeleteOperationRangeAsync, () => SelectedOperationRange != null);
            DeleteRangeParameterCommand = new RelayCommand(DeleteRangeParameterAsync, () => SelectedRangeParameter != null);
            RemoveModuleConveyorInTopologyCommand = new RelayCommand(RemoveModuleConveyorInTopology, CanRemoveModuleConveyorInTopology);
            RevertConveyorLinkChangeCommand = new RelayCommand(() => SelectConveyorLink(SelectedConveyorLink), ConveyorLinkChangeCanExecute);
            RevertTopologyChangesCommand = new RelayCommand(async () => await SelectTopologyAsync(SelectedTopology), () => WorkingCopyTopology?.IsDirty ?? false);
            RevertModuleConveyorChangesCommand = new RelayCommand(async () => await SelectModuleConveyorAsync(SelectedModuleConveyor), () => WorkingCopyModuleConveyor?.IsDirty ?? false);
            RevertResourcesChangesCommand = new RelayCommand(async () => await SelectResourceAsync(SelectedResource), () => WorkingCopyResource?.IsDirty ?? false);
            RevertApplicationChangesCommand = new RelayCommand(async () => await SelectApplicationAsync(SelectedApplication), () => WorkingCopyApplication?.IsDirty ?? false);
            RefreshCommand = new RelayCommand(async () => await LoadInitialDataAsync());
            SaveTopologyCommand = new RelayCommand(async () => await SaveTopologyAsync(), () => WorkingCopyTopology?.IsDirty ?? false);
            SaveModuleConveyorCommand = new RelayCommand(async () => await SaveModuleConveyorAsync(), () => WorkingCopyModuleConveyor?.IsDirty ?? false);
            SaveResourceCommand = new RelayCommand(async () => await SaveResourceAsync(), () => WorkingCopyResource?.IsDirty ?? false);
            SaveApplicationCommand = new RelayCommand(async () => await SaveApplicationAsync(), () => WorkingCopyApplication?.IsDirty ?? false);
            WorkingCopyConveyorResourceChangedCommand = new RelayCommand<object>(ModuleConveyorResourceChanged);
            WorkingCopyResourceApplicationChangedCommand = new RelayCommand<object>(ResourceApplicationChanged);
            SelectedApplicationOperationChangedCommand = new RelayCommand<object>(SelectedApplicationOperationsChanged);
            SelectedOperationRangeChangedCommand = new RelayCommand<object>(SelectedOperationRangeChanged);
            SelectedRangeParameterChangedCommand = new RelayCommand<object>(SeletedRangeParameterChanged);
            TopologyNameChangedCommand = new RelayCommand(() => { if (WorkingCopyTopology != null) WorkingCopyTopology.IsDirty = true; });
            ResourceDetailsChangedCommand = new RelayCommand(() => { if (WorkingCopyResource != null) WorkingCopyResource.IsDirty = true; });
            ApplicationDetailsChangedCommand = new RelayCommand(() => { if (WorkingCopyApplication != null) WorkingCopyApplication.IsDirty = true; });
        }

        private void SetModuleConveyorsForTopology()
        {
            ModuleConveyorsInTopology = new ObservableCollection<ModuleConveyor>(ModuleConveyors.Where(mc => WorkingCopyTopology.ConveyorIds.Contains(mc.ModuleConveyorId.Value)));
            ModuleConveyorsNotInTopology = new ObservableCollection<ModuleConveyor>(ModuleConveyors.Where(mc => !WorkingCopyTopology.ConveyorIds.Contains(mc.ModuleConveyorId.Value)));
        }

        private void AddModuleConveyorInTopology()
        {
            topologyDataService.AddModuleConveyorToTopology(WorkingCopyTopology, SelectedTopologyModuleConveyor);
            SetModuleConveyorsForTopology();
        }

        private bool CanAddModuleConveyorInTopology() => ModuleConveyorsNotInTopology.Contains(SelectedTopologyModuleConveyor);

        private void RemoveModuleConveyorInTopology()
        {
            topologyDataService.RemoveModuleConveyorToTopology(WorkingCopyTopology, SelectedTopologyModuleConveyor);
            SetModuleConveyorsForTopology();
        }

        private bool CanRemoveModuleConveyorInTopology() => ModuleConveyorsInTopology.Contains(SelectedTopologyModuleConveyor);

        private async Task CreateTopologyAsync()
        {
            var top = new TopologyEx { IsNew = true, IsDirty = true };
            Topologies.Add(top);
            await SelectTopologyAsync(top);
        }

        private void CreateApplication()
        {
            var app = new ApplicationEx { IsDirty = true, IsNew = true };
            Applications.Add(app);
            Task.Run(async () => await SelectApplicationAsync(app));
        }

        private void CreateResource()
        {
            var res = new ResourceEx { IsDirty = true, IsNew = true };
            Resources.Add(res);
            Task.Run(async () => await SelectResourceAsync(res)).Wait();
        }

        private void CreateModuleConveyor()
        {
            var mod = new ModuleConveyorEx { IsDirty = true, IsNew = true };
            ModuleConveyors.Add(mod);
            Task.Run(async () => await SelectModuleConveyorAsync(mod)).Wait();
        }

        private void CreateRangeParameter()
        {
            var rangeParamenter = new RangeParameter { Name = "New" };
            RangeParameters.Add(rangeParamenter);
            ((ICollection<RangeParameter>)SelectedOperationRange.Parameters).Add(rangeParamenter);
        }

        private void CreateOperationRange()
        {
            var range = new Range { Name = "New" };
            OperationRanges.Add(range);
            ((ICollection<Range>)SelectedApplicationOperation.SupportedRanges).Add(range);
        }

        private void CreateApplicationOperation()
        {
            var operation = new ApplicationOperation { OperationName = "New" };
            ApplicationOperations.Add(operation);
            ((ICollection<ApplicationOperation>)SelectedApplication.SupportedOperations).Add(operation);
        }

        private void DeleteRangeParameterAsync()
        {
            RangeParameters.Remove(SelectedRangeParameter);
            ((ICollection<RangeParameter>)SelectedOperationRange.Parameters).Remove(SelectedRangeParameter);
            SelectedRangeParameter = null;
        }

        private void DeleteOperationRangeAsync()
        {
            OperationRanges.Remove(SelectedOperationRange);
            ((ICollection<Range>)SelectedApplicationOperation.SupportedRanges).Remove(SelectedOperationRange);
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
            RangeParameters = null;
        }

        private void DeleteApplicationOperationAsync()
        {
            ApplicationOperations.Remove(SelectedApplicationOperation);
            ((ICollection<ApplicationOperation>)SelectedApplication.SupportedOperations).Remove(SelectedApplicationOperation);
            SelectedApplicationOperation = null;
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
            OperationRanges = null;
            RangeParameters = null;
        }

        private async void DeleteApplicationAsync()
        {
            await topologyDataService.DeleteApplicationAsync(SelectedApplication);
            SelectedApplication = null;
            WorkingCopyApplication = null;
            SelectedApplicationOperation = null;
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
        }

        private async void DeleteResourceAsync()
        {
            await topologyDataService.DeleteResourceAsync(SelectedResource);
            SelectedResource = null;
            WorkingCopyResource = null;
        }

        private async void DeleteModuleConveyorAsync()
        {
            await topologyDataService.DeleteModuleConveyorAsync(SelectedModuleConveyor);
            SelectedModuleConveyor = null;
            WorkingCopyModuleConveyor = null;
        }

        private async Task DeleteTopologyAsync()
        {
            await topologyDataService.DeleteTopologyAsync(SelectedTopology);
            SelectedTopology = null;
            WorkingCopyTopology = null;
        }

        public async Task SelectTopologyAsync(TopologyEx topology)
        {
            if (!topology.IsNew && topology.IsLoaded == false)
            {
                topology = await topologyDataService.RefreshTopologyAsync(topology.TopologyId);
            }
            SelectedTopology = topology;
            WorkingCopyTopology = new TopologyEx(SelectedTopology);
            SelectedTopologyModuleConveyor = null;
            var conveyorCollection = new ObservableCollection<Services.ConveyorLink>();
            foreach (var top in SelectedTopology.ConveyorLinkIds)
            {
                foreach (var bottom in top.Value)
                {
                    var source = top.Key;
                    var port = bottom.Key;
                    var destination = bottom.Value;
                    conveyorCollection.Add(new Services.ConveyorLink(source, port, destination));
                }
            }
            ConveyorLinks = conveyorCollection;
            SetModuleConveyorsForTopology();
            SelectedModuleConveyor = null;
            SelectedResource = null;
            SelectedApplication = null;
            SelectedApplicationOperation = null;
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
            WorkingCopyModuleConveyor = null;
            WorkingCopyResource = null;
            WorkingCopyApplication = null;
        }

        public async Task SelectModuleConveyorAsync(ModuleConveyorEx moduleConveyor)
        {
            if (!moduleConveyor.IsNew && !moduleConveyor.IsLoaded)
            {
                moduleConveyor = await topologyDataService.RefreshModuleConveyorAsync(moduleConveyor.ModuleConveyorId.Value);
            }
            SelectedModuleConveyor = moduleConveyor;
            WorkingCopyModuleConveyor = new ModuleConveyorEx(moduleConveyor);
            RaisePropertyChanged(nameof(ModuleConveyorResourceIndex));
            SelectedTopology = null;
            SelectedTopologyModuleConveyor = null;
            SelectedResource = null;
            SelectedApplication = null;
            SelectedApplicationOperation = null;
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
            WorkingCopyTopology = null;
            WorkingCopyTopologyModuleConveyor = null;
            WorkingCopyResource = null;
            WorkingCopyApplication = null;
        }

        internal async Task SelectResourceAsync(ResourceEx resource)
        {
            if (!resource.IsNew && !resource.IsLoaded)
            {
                resource = await topologyDataService.LoadResourceAsync(resource.ResourceId);
            }
            SelectedResource = resource;
            WorkingCopyResource = new ResourceEx(resource);
            SelectedTopology = null;
            SelectedTopologyModuleConveyor = null;
            SelectedModuleConveyor = null;
            SelectedApplication = null;
            SelectedApplicationOperation = null;
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
            WorkingCopyTopology = null;
            WorkingCopyTopologyModuleConveyor = null;
            WorkingCopyModuleConveyor = null;
            WorkingCopyApplication = null;
        }

        internal async Task SelectApplicationAsync(ApplicationEx application)
        {
            if (!application.IsNew && !application.IsLoaded)
            {
                application = await topologyDataService.LoadApplicationAsync(application.ApplicationId);
            }
            SelectedApplication = application;
            WorkingCopyApplication = new ApplicationEx(application);
            ApplicationOperations = new ObservableCollection<ApplicationOperation>(WorkingCopyApplication.SupportedOperations);
            WorkingCopyApplication.SupportedOperations = ApplicationOperations;
            SelectedTopology = null;
            SelectedTopologyModuleConveyor = null;
            SelectedModuleConveyor = null;
            SelectedResource = null;
            SelectedApplicationOperation = null;
            SelectedOperationRange = null;
            SelectedRangeParameter = null;
            WorkingCopyTopology = null;
            WorkingCopyTopologyModuleConveyor = null;
            WorkingCopyModuleConveyor = null;
            WorkingCopyResource = null;
            OperationRanges = null;
            RangeParameters = null;
        }

        internal async Task LoadInitialDataAsync()
        {
            await topologyDataService.RefreshAllAsync();
            RaisePropertyChanged(nameof(Topologies));
            RaisePropertyChanged(nameof(ModuleConveyors));
            RaisePropertyChanged(nameof(Resources));
            RaisePropertyChanged(nameof(Applications));
        }

        private void CreateConveyorLink()
        {
            var link = new Services.ConveyorLink();
            ConveyorLinks.Add(link);
            SelectedConveyorLink = link;
        }

        internal void SelectConveyorLink(Services.ConveyorLink selectedLink)
        {
            SelectedConveyorLink = selectedLink;
            WorkingCopyConveyorLink = new Services.ConveyorLink(selectedLink);
            ApplyConveyorLinkChangeCommand.RaiseCanExecuteChanged();
            RevertConveyorLinkChangeCommand.RaiseCanExecuteChanged();
        }

        private async Task SaveTopologyAsync()
        {
            if (await topologyDataService.PublishTopologyAsync(WorkingCopyTopology))
            {
                var index = Topologies.IndexOf(SelectedTopology);
                Topologies[index] = WorkingCopyTopology;
                SelectedTopology = WorkingCopyTopology;
                SaveTopologyCommand.RaiseCanExecuteChanged();
            }
        }

        private async Task SaveModuleConveyorAsync()
        {
            if (await topologyDataService.PublishModuleConveyorAsync(WorkingCopyModuleConveyor))
            {
                var index = ModuleConveyors.IndexOf(SelectedModuleConveyor);
                ModuleConveyors[index] = WorkingCopyModuleConveyor;
                SelectedModuleConveyor = WorkingCopyModuleConveyor;
                SaveModuleConveyorCommand.RaiseCanExecuteChanged();
            }
        }

        private async Task SaveApplicationAsync()
        {
            if (await topologyDataService.PublishApplicationAsync(WorkingCopyApplication))
            {
                var index = Applications.IndexOf(SelectedApplication);
                Applications[index] = WorkingCopyApplication;
                SelectedApplication = WorkingCopyApplication;
                SaveApplicationCommand.RaiseCanExecuteChanged();
            }
        }

        private async Task SaveResourceAsync()
        {
            if (await topologyDataService.PublishResourceAsync(WorkingCopyResource))
            {
                var index = Resources.IndexOf(SelectedResource);
                Resources[index] = WorkingCopyResource;
                SelectedResource = WorkingCopyResource;
                SaveResourceCommand.RaiseCanExecuteChanged();
            }
        }

        private bool ConveyorLinkChangeCanExecute() => SelectedConveyorLink != null;

        private void UpdateConveyorLink()
        {
            if (SelectedConveyorLink != null)
            {
                var index = ConveyorLinks.IndexOf(SelectedConveyorLink);
                ConveyorLinks[index] = WorkingCopyConveyorLink;
                SelectedConveyorLink = WorkingCopyConveyorLink;
                topologyDataService.SoftUpdateConveyorLink(WorkingCopyTopology, SelectedConveyorLink);
            }
        }

        private void ModuleConveyorResourceChanged(object newItem)
        {
            if (newItem is ResourceEx res)
            {
                if (res != WorkingCopyModuleConveyor.Resource)
                {
                    WorkingCopyModuleConveyor.Resource = res;
                    WorkingCopyModuleConveyor.ResourceId = res.ResourceId;
                    WorkingCopyModuleConveyor.IsDirty = true;
                }
            }
            else
            {
                WorkingCopyModuleConveyor.Resource = null;
                WorkingCopyModuleConveyor.ResourceId = null;
                WorkingCopyModuleConveyor.IsDirty = true;
            }
        }

        private void ResourceApplicationChanged(object newItem)
        {
            if (newItem is ApplicationEx app)
            {
                if (app != WorkingCopyResource.Application)
                {
                    WorkingCopyResource.Application = app;
                    WorkingCopyResource.ApplicationId = app.ApplicationId;
                    WorkingCopyResource.IsDirty = true;
                }
            }
            else
            {
                WorkingCopyResource.Application = null;
                WorkingCopyResource.ApplicationId = null;
                WorkingCopyResource.IsDirty = true;
            }
        }

        private void SelectedApplicationOperationsChanged(object selectedItem)
        {
            if (selectedItem is ApplicationOperation applicationOperation)
            {
                if (SelectedApplicationOperation != null)
                {
                    SelectedApplicationOperation.SupportedRanges = OperationRanges;
                }
                SelectedApplicationOperation = applicationOperation;
                SelectedOperationRange = null;
                SelectedRangeParameter = null;
                OperationRanges = new ObservableCollection<Range>(applicationOperation.SupportedRanges);
            }
        }

        private void SelectedOperationRangeChanged(object selectedItem)
        {
            if (selectedItem is Range range)
            {
                if (SelectedOperationRange != null)
                {
                    SelectedOperationRange.Parameters = RangeParameters;
                }
                SelectedOperationRange = range;
                SelectedRangeParameter = null;
                RangeParameters = new ObservableCollection<RangeParameter>(range.Parameters);
            }
        }

        private void SeletedRangeParameterChanged(object selectedItem)
        {
            if (selectedItem is RangeParameter rangeParameter)
            {
                SelectedRangeParameter = rangeParameter;
            }
        }
    }
}