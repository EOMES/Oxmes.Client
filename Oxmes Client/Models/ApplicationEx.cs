﻿using Newtonsoft.Json;
using Oxmes.Topology.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Models
{
    public class ApplicationEx : Application
    {
        [JsonIgnore]
        public bool IsDirty { get; set; } = false;

        [JsonIgnore]
        public bool IsNew { get; set; } = false;

        [JsonIgnore]
        public bool IsLoaded { get; set; } = false;

        public ApplicationEx()
        {
        }

        public ApplicationEx(ApplicationEx application)
        {
            ApplicationId = application.ApplicationId;
            ApplicationName = application.ApplicationName;
            IsDirty = application.IsDirty;
            IsLoaded = application.IsLoaded;
            IsNew = application.IsNew;
            Resource = application.Resource;
            ResourceId = application.ResourceId;
            SupportedOperations = application.SupportedOperations;
        }
    }
}