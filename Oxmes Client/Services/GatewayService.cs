﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Oxmes.Core.ServiceUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Services
{
    public class GatewayService
    {
        private readonly IConfiguration configuration;
        private readonly ILogger<GatewayService> Logger;

        internal IServiceClient ServiceClient { get; set; }

        internal HttpClient GatewayHttpClient { get; set; }

        public GatewayService(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            this.configuration = configuration;
            Logger = loggerFactory.CreateLogger<GatewayService>();

            RefreshGatewayHttpClient();
        }

        internal void RefreshGatewayHttpClient()
        {
            var uriString = configuration["GatewayUrl"];
            if (uriString != null)
            {
                GatewayHttpClient = new HttpClient { BaseAddress = new Uri($"{configuration["GatewayUrl"]}/api/") };
            }
        }
    }
}
