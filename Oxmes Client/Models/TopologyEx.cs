﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Models
{
    public class TopologyEx : Topology.Models.Topology
    {
        [JsonIgnore]
        public bool IsLoaded { get; set; } = false;

        [JsonIgnore]
        public bool IsNew { get; set; } = false;

        [JsonIgnore]
        public bool IsDirty { get; set; } = false;

        public TopologyEx()
        {
            ConveyorIds = new int[] { };
            ConveyorLinkIds = new Dictionary<int, IDictionary<int, int>>();
        }

        public TopologyEx(TopologyEx topologyToClone)
        {
            if (topologyToClone == null) throw new ArgumentNullException(nameof(topologyToClone));

            TopologyId = topologyToClone.TopologyId;
            TopologyName = topologyToClone.TopologyName;
            IsActive = topologyToClone.IsActive;
            IsLoaded = topologyToClone.IsLoaded;
            IsNew = topologyToClone.IsNew;
            IsDirty = topologyToClone.IsDirty;
            ConveyorIds = new int[topologyToClone.ConveyorIds.Length];
            topologyToClone.ConveyorIds?.CopyTo(ConveyorIds, 0);
            ConveyorLinkIds = new Dictionary<int, IDictionary<int, int>>(topologyToClone.ConveyorLinkIds);
        }
    }
}
