﻿using Oxmes.Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Oxmes.Client.Views.TemplateSelectors
{
    public class ConveyorLinkTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is ConveyorLink cLink && container is FrameworkElement element)
            {
                if (cLink.SourceId == null)
                {
                    return (DataTemplate)element.FindResource("ConveyorLinksNullDataTemplate");
                }
                else
                {
                    return (DataTemplate)element.FindResource("ConveyorLinksDataTemplate");
                }
            }
            return null;
        }
    }
}
