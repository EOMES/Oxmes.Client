﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oxmes.Client.UserControls
{
    /// <summary>
    /// Interaction logic for PropertiesFrameUserControl.xaml
    /// </summary>
    public partial class PropertiesFrameUserControl : UserControl
    {
        public Frame PropertiesFrame => PropertiesFrameInternal;
        public event EventHandler ControlHidden;

        public PropertiesFrameUserControl()
        {
            InitializeComponent();
        }

        public void Expand()
        {
            VisualStateManager.GoToState(this, PropertiesVisibleState.Name, false);
        }

        public void Hide()
        {
            VisualStateManager.GoToState(this, Default.Name, true);
            ControlHidden?.Invoke(this, null);
        }

        private void HidePropertiesButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }
    }
}
