﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Oxmes.Client.Models;
using Oxmes.Topology.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Services
{
    using TopModel = Oxmes.Topology.Models;

    public class TopologyDataService : DataServiceBase
    {
        private readonly ILogger Logger;

        public ObservableCollection<TopologyEx> Topologies { get; set; }
        public ObservableCollection<ModuleConveyorEx> ModuleConveyors { get; set; }
        public ObservableCollection<ResourceEx> Resources { get; set; }
        public ObservableCollection<ApplicationEx> Applications { get; set; }

        public TopologyDataService(GatewayService gatewayService, ILoggerFactory loggerFactory) : base(gatewayService, loggerFactory)
        {
            Logger = loggerFactory.CreateLogger<TopologyDataService>();
        }

        public async Task RefreshAllAsync()
        {
            var tops = LoadTopologiesAsync();
            var mods = LoadModuleConveyorsAsync();
            var res = LoadResourcesAsync();
            var apps = LoadApplicationsAsync();

            await tops;
            await mods;
            await res;
            await apps;
        }

        private void LoadTopologies()
        {
            Task.Run(LoadTopologiesAsync).Wait();
        }

        private async Task LoadTopologiesAsync()
        {
            await GetRequestAsync("topologies", async (response) =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var tops = await response.Content.ReadAsStringAsync();
                    Topologies = JsonConvert.DeserializeObject<ObservableCollection<TopologyEx>>(tops);
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Logger.LogWarning("No topologies found");
                }
            });
        }

        public async Task RefreshTopologiesAsync() => await LoadTopologiesAsync();

        public async Task<TopologyEx> RefreshTopologyAsync(int id)
        {
            TopologyEx topology = null;
            await GetRequestAsync($"topologies/{id}", async (response) =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var top = await response.Content.ReadAsStringAsync();
                    topology = JsonConvert.DeserializeObject<TopologyEx>(top);
                    topology.IsLoaded = true;
                    var oldTop = Topologies.FirstOrDefault((t) => t.TopologyId == id);
                    if (oldTop != null)
                    {
                        var index = Topologies.IndexOf(oldTop);
                        Topologies[index] = topology;
                    }
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Logger.LogWarning($"No topology with ID {id} found");
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    Logger.LogWarning("Received Bad Request. Trouble with the API");
                }
            });
            return topology;
        }

        internal async Task DeleteTopologyAsync(TopologyEx topology)
        {
            if (topology.IsNew)
            {
                Topologies.Remove(topology);
            }
            else
            {
                await DeleteRequestAsync($"topologies/{topology.TopologyId}", response =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        Topologies.Remove(topology);
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No topology with ID {topology.TopologyId} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
        }

        internal async Task DeleteModuleConveyorAsync(ModuleConveyorEx moduleConveyor)
        {
            if (moduleConveyor.IsNew)
                ModuleConveyors.Remove(moduleConveyor);

            if (!moduleConveyor.IsNew)
            {
                await DeleteRequestAsync($"moduleconveyors/{moduleConveyor.ModuleConveyorId}", response =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        ModuleConveyors.Remove(moduleConveyor);
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No module conveyor with ID {moduleConveyor.ModuleConveyorId} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
        }

        internal async Task DeleteResourceAsync(ResourceEx resource)
        {
            if (resource.IsNew)
                Resources.Remove(resource);

            if (!resource.IsNew)
            {
                await DeleteRequestAsync($"resources/{resource.ResourceId}", response =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        Resources.Remove(resource);
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No resource with ID {resource.ResourceId} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
        }

        internal async Task DeleteApplicationAsync(ApplicationEx application)
        {
            if (application.IsNew)
                Applications.Remove(application);

            if (!application.IsNew)
            {
                await DeleteRequestAsync($"applications/{application.ApplicationId}", response =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        Applications.Remove(application);
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No application with ID {application.ApplicationId} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
        }

        private void LoadModuleConveyors() => Task.Run(LoadModuleConveyorsAsync).Wait();

        private async Task LoadModuleConveyorsAsync()
        {
            await GetRequestAsync("moduleconveyors", async (response) =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var mods = await response.Content.ReadAsStringAsync();
                    ModuleConveyors = JsonConvert.DeserializeObject<ObservableCollection<ModuleConveyorEx>>(mods);
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Logger.LogWarning("No moduleconveyors found");
                }
            });
        }

        internal async Task<ModuleConveyorEx> RefreshModuleConveyorAsync(int id)
        {
            ModuleConveyorEx moduleConveyor = null;
            await GetRequestAsync($"moduleconveyors/{id}", async (response) =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var mod = await response.Content.ReadAsStringAsync();
                    moduleConveyor = JsonConvert.DeserializeObject<ModuleConveyorEx>(mod);
                    moduleConveyor.IsLoaded = true;
                    var oldMod = ModuleConveyors.FirstOrDefault(mc => mc.ModuleConveyorId == id);
                    if (oldMod != null)
                    {
                        var index = ModuleConveyors.IndexOf(oldMod);
                        ModuleConveyors[index] = moduleConveyor;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No module conveyor with ID {id} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                }
            });

            return moduleConveyor;
        }

        internal void AddModuleConveyorToTopology(TopologyEx workingCopyTopology, ModuleConveyor selectedModuleConveyor)
        {
            var list = workingCopyTopology.ConveyorIds.ToList();
            list.Add(selectedModuleConveyor.ModuleConveyorId.Value);
            workingCopyTopology.ConveyorIds = list.ToArray();
            workingCopyTopology.IsDirty = true;
        }

        internal void RemoveModuleConveyorToTopology(TopologyEx workingCopyTopology, ModuleConveyor selectedModuleConveyor)
        {
            var list = workingCopyTopology.ConveyorIds.ToList();
            list.Remove(selectedModuleConveyor.ModuleConveyorId.Value);
            workingCopyTopology.ConveyorIds = list.ToArray();
            workingCopyTopology.IsDirty = true;
        }

        internal void SoftUpdateConveyorLink(TopologyEx topology, ConveyorLink updatedLink)
        {
            if (!updatedLink.SourceId.HasValue) throw new ArgumentNullException(nameof(updatedLink.SourceId));
            if (!updatedLink.PortNumber.HasValue) throw new ArgumentNullException(nameof(updatedLink.PortNumber));
            if (!updatedLink.DestinationId.HasValue) throw new ArgumentNullException(nameof(updatedLink.DestinationId));

            var sourceId = updatedLink.SourceId.Value;
            var portNumber = updatedLink.PortNumber.Value;
            var destinationId = updatedLink.DestinationId.Value;

            if (!topology.ConveyorLinkIds.ContainsKey(sourceId))
            {
                topology.ConveyorLinkIds[sourceId] = new Dictionary<int, int>();
            }
            topology.ConveyorLinkIds[sourceId][portNumber] = destinationId;
            topology.IsDirty = true;
        }

        internal async Task LoadResourcesAsync()
        {
            await GetRequestAsync("resources", async response =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var res = await response.Content.ReadAsStringAsync();
                    Resources = JsonConvert.DeserializeObject<ObservableCollection<ResourceEx>>(res);
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Logger.LogWarning("No resources found");
                }
            });
        }

        internal async Task<ResourceEx> LoadResourceAsync(int id)
        {
            ResourceEx resource = null;
            await GetRequestAsync($"resources/{id}", async response =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var res = await response.Content.ReadAsStringAsync();
                    resource = JsonConvert.DeserializeObject<ResourceEx>(res);
                    resource.IsLoaded = true;
                    var oldRes = Resources.FirstOrDefault(r => r.ResourceId == id);
                    if (oldRes != null)
                    {
                        var index = Resources.IndexOf(oldRes);
                        Resources[index] = resource;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No resource with ID {id} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                }
            });

            return resource;
        }

        internal async Task LoadApplicationsAsync()
        {
            await GetRequestAsync("applications", async response =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var apps = await response.Content.ReadAsStringAsync();
                    Applications = JsonConvert.DeserializeObject<ObservableCollection<ApplicationEx>>(apps);
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Logger.LogWarning("No application found");
                }
            });
        }

        internal async Task<ApplicationEx> LoadApplicationAsync(int id)
        {
            ApplicationEx application = null;
            await GetRequestAsync($"applications/{id}", async response =>
            {
                if (response.IsSuccessStatusCode)
                {
                    var app = await response.Content.ReadAsStringAsync();
                    application = JsonConvert.DeserializeObject<ApplicationEx>(app);
                    application.IsLoaded = true;
                    var oldApp = Applications.FirstOrDefault(r => r.ApplicationId == id);
                    if (oldApp != null)
                    {
                        var index = Applications.IndexOf(oldApp);
                        Applications[index] = application;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning($"No application with ID {id} found");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                }
            });

            return application;
        }

        internal async Task<bool> PublishTopologyAsync(TopologyEx topology)
        {
            bool success = false;
            if (topology.IsNew)
            {
                await PostRequestAsync($"topologies", topology, async (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var topJson = await response.Content.ReadAsStringAsync();
                        var top = JsonConvert.DeserializeObject<TopologyEx>(topJson);
                        topology.IsDirty = false;
                        topology.IsNew = false;
                        topology.TopologyId = top.TopologyId;
                        success = true;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update topology, but topology has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
            else if (topology.IsDirty)
            {
                await PutRequestAsync($"topologies/{topology.TopologyId}", topology, (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        topology.IsDirty = false;
                        success = true;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update topology, but topology has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }

            return success;
        }

        internal async Task<bool> PublishModuleConveyorAsync(ModuleConveyorEx moduleConveyor)
        {
            bool success = false;
            if (moduleConveyor.IsNew)
            {
                await PostRequestAsync($"moduleconveyors", moduleConveyor, async (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var modJson = await response.Content.ReadAsStringAsync();
                        var mod = JsonConvert.DeserializeObject<ModuleConveyorEx>(modJson);
                        moduleConveyor.IsDirty = false;
                        moduleConveyor.IsNew = false;
                        moduleConveyor.ModuleConveyorId = mod.ModuleConveyorId;
                        success = true;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update module conveyor, but module conveyor has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
            else if (moduleConveyor.IsDirty)
            {
                await PutRequestAsync($"moduleconveyors/{moduleConveyor.ModuleConveyorId}", moduleConveyor, (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        moduleConveyor.IsDirty = false;
                        success = true;

                        // Invalidate cache any other module with the same resource (it will have changed)
                        foreach (var mc in ModuleConveyors) if (mc.ResourceId == moduleConveyor.ResourceId) mc.IsLoaded = false;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update module conveyor, but module conveyor has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }

            return success;
        }

        internal async Task PublishUpdatesAsync()
        {
            foreach (var top in Topologies.Where(t => t.IsNew))
            {
                await PostRequestAsync($"topologies", top, async (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var topJson = await response.Content.ReadAsStringAsync();
                        var topology = JsonConvert.DeserializeObject<TopologyEx>(topJson);
                        top.IsDirty = false;
                        top.IsNew = false;
                        top.TopologyId = topology.TopologyId;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update topology, but topology has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }

            foreach (var top in Topologies.Where(t => t.IsDirty && !t.IsNew))
            {
                await PutRequestAsync($"topologies/{top.TopologyId}", top, (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        top.IsDirty = false;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update topology, but topology has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
        }

        internal async Task<bool> PublishApplicationAsync(ApplicationEx application)
        {
            bool success = false;
            if (application.IsNew)
            {
                await PostRequestAsync($"applications", application, async (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var appJson = await response.Content.ReadAsStringAsync();
                        var res = JsonConvert.DeserializeObject<ApplicationEx>(appJson);
                        application.IsDirty = false;
                        application.IsNew = false;
                        application.ApplicationId = res.ApplicationId;
                        success = true;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update application, but application has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
            else if (application.IsDirty)
            {
                await PutRequestAsync($"applications/{application.ApplicationId}", application, (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        application.IsDirty = false;
                        success = true;
                        application.IsLoaded = false;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update application, but application has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }

            return success;
        }

        internal async Task<bool> PublishResourceAsync(ResourceEx resource)
        {
            bool success = false;
            if (resource.IsNew)
            {
                await PostRequestAsync($"resources", resource, async (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var resJson = await response.Content.ReadAsStringAsync();
                        var res = JsonConvert.DeserializeObject<ResourceEx>(resJson);
                        resource.IsDirty = false;
                        resource.IsNew = false;
                        resource.ResourceId = res.ResourceId;
                        success = true;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update resource, but resource has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }
            else if (resource.IsDirty)
            {
                await PutRequestAsync($"resources/{resource.ResourceId}", resource, (response) =>
                {
                    if (response.IsSuccessStatusCode)
                    {
                        resource.IsDirty = false;
                        success = true;

                        // Invalidate cache any other module with the same resource (it will have changed)
                        foreach (var r in Resources) if (r.ApplicationId == resource.ApplicationId) r.IsLoaded = false;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Logger.LogWarning("Tried to update resource, but resource has already been deleted");
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Logger.LogWarning("Received Bad Request. Trouble with the API");
                    }
                });
            }

            return success;
        }
    }

    public class ConveyorLink
    {
        public int? SourceId { get; set; }
        public int? PortNumber { get; set; }
        public int? DestinationId { get; set; }

        public ConveyorLink()
        {
        }

        public ConveyorLink(int sourceId, int portNumber, int destinationId)
        {
            SourceId = sourceId;
            PortNumber = portNumber;
            DestinationId = destinationId;
        }

        public ConveyorLink(ConveyorLink linkToCopy)
        {
            this.SourceId = linkToCopy.SourceId;
            this.PortNumber = linkToCopy.PortNumber;
            this.DestinationId = linkToCopy.DestinationId;
        }
    }
}