﻿using Newtonsoft.Json;
using Oxmes.Topology.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxmes.Client.Models
{
    public class ResourceEx : Resource
    {
        [JsonIgnore]
        public bool IsDirty { get; set; } = false;

        [JsonIgnore]
        public bool IsNew { get; set; } = false;

        [JsonIgnore]
        public bool IsLoaded { get; set; } = false;

        public ResourceEx()
        {
        }

        public ResourceEx(ResourceEx resource)
        {
            ResourceId = resource.ResourceId;
            ResourceName = resource.ResourceName;
            ModuleConveyorId = resource.ModuleConveyorId;
            Application = resource.Application;
            ApplicationId = resource.ApplicationId;
            IsDirty = resource.IsDirty;
            IsLoaded = resource.IsLoaded;
            IsNew = resource.IsNew;
        }
    }
}