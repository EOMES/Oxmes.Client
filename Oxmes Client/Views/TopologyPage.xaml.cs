﻿using Oxmes.Client.Models;
using Oxmes.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oxmes.Client.Views
{
    /// <summary>
    /// Interaction logic for TopologyPage.xaml
    /// </summary>
    public partial class TopologyPage : Page
    {
        private TopologyViewModel TopologyViewModel => DataContext as TopologyViewModel;

        public TopologyPage()
        {
            InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await TopologyViewModel.LoadInitialDataAsync();
            PropertiesControl.ControlHidden += PropertiesControl_ControlHidden;
        }

        private void PropertiesControl_ControlHidden(object sender, EventArgs e)
        {
            TopologyListView.SelectedItem = null;
        }

        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1 && e.AddedItems[0] is TopologyEx top)
            {
                PropertiesControl.Expand();
                await TopologyViewModel.SelectTopologyAsync(top);
                PropertiesControl.PropertiesFrame.Navigate(new TopologyPropertiesPage());
            }
        }

        private async void ModulesConveyorListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1 && e.AddedItems[0] is ModuleConveyorEx moduleConveyor)
            {
                PropertiesControl.Expand();
                await TopologyViewModel.SelectModuleConveyorAsync(moduleConveyor);
                PropertiesControl.PropertiesFrame.Navigate(new ModuleConveyorsPage());
            }
        }

        private async void ResourcesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1 && e.AddedItems[0] is ResourceEx resource)
            {
                PropertiesControl.Expand();
                await TopologyViewModel.SelectResourceAsync(resource);
                PropertiesControl.PropertiesFrame.Navigate(new ResourcePropertiesPage());
            }
        }

        private async void ApplicationsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1 && e.AddedItems[0] is ApplicationEx application)
            {
                PropertiesControl.Expand();
                await TopologyViewModel.SelectApplicationAsync(application);
                PropertiesControl.PropertiesFrame.Navigate(new ApplicationPropertiesPage());
            }
        }
    }
}